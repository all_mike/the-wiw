package com.example.SmokeyTiles;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by ynden on 08/06/16.
 */
public class SmokeyTilesScores extends Activity {

    private Button mReturnButton;
    private SmokeyTilesScores ownRef;
    private ListView mScores;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scores);
        ownRef = this;

        mScores = (ListView)findViewById(R.id.idListViewScores);

        ArrayList<String> scores = new ArrayList<String>();

        try
        {
            SmokeyDB db = SmokeyDB.getInstance();
            Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM " + SmokeyDB.TABLE_REALIZED + ";", null);

            for (int i = 0; i < c.getCount(); ++i)
            {
                if (!c.moveToPosition(i))
                    break;

                int idplayer = c.getInt(0);
                int idpart = c.getInt(1);
                String d = c.getString(2);
                int score = c.getInt(3);

                // get player name
                String playerName = "!";
                Cursor cpn = db.getReadableDatabase().rawQuery("SELECT * FROM " + SmokeyDB.TABLE_PLAYER + " WHERE " + SmokeyDB.COL_PLAYER_ID + " = " + idplayer + ";", null);
                cpn.moveToFirst();
                playerName = cpn.getString(1);

                // get partition name
                String partName = "!";
                cpn = db.getReadableDatabase().rawQuery("SELECT * FROM " + SmokeyDB.TABLE_PARTITION + " WHERE " + SmokeyDB.COL_PARTITION_ID + " = " + idpart + ";", null);
                cpn.moveToFirst();
                partName = cpn.getString(1);

                cpn.close();

                scores.add("PLAYER : " + playerName + "   SONG : " + partName + "  SCORE : " + score + "   DATE : " + d);
            }

            c.close();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item, scores);
            mScores.setAdapter(adapter);
        }
        catch (Exception e)
        {
            Log.i("sql", e.getMessage());
        }

        mReturnButton = (Button)findViewById(R.id.idButtonReturn);

        mReturnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backToMainIntent = new Intent(ownRef, SmokeyTilesMain.class);
                startActivity(backToMainIntent);
                ownRef.finish();
            }
        });
    }
}
