package com.example.SmokeyTiles;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;

/**
 * Created by ynden on 01/06/16.
 */
public class SmokeyTilesMain extends Activity
{
    private Button mPlayButton;
    private Button mScoresButton;
    private Button mQuitButton;
    private SmokeyTilesMain ownRef;
    private ListView mMusicListView;
    private int mMusicIdSelected;
    private EditText mEditPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ownRef = this;

        mMusicIdSelected = -1;

        /*
            Example requetes db
         */
        /*
        SmokeyDB.setContext(getApplicationContext());
        try {
            SmokeyDB db = SmokeyDB.getInstance();

            Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM " + SmokeyDB.TABLE_PLAYER + ";", null);

            Log.i("sql", "count : " + c.getCount());

            String s = "INSERT INTO " + SmokeyDB.TABLE_PLAYER + "(" + SmokeyDB.COL_PLAYER_ID +
                    ", " + SmokeyDB.COL_PLAYER_NAME + ") VALUES(" + 0 + ", " + "\"VAL\"" + ");";

            Log.i("sql", s);

            db.getWritableDatabase().execSQL(s);

            Cursor c2 = db.getReadableDatabase().rawQuery("SELECT * FROM " + SmokeyDB.TABLE_PLAYER + ";", null);

            Log.i("sql", "count : " + c2.getCount());

        }
        catch (Exception e)
        {
            Log.i("sql", e.getMessage());
        }
        */

        mMusicListView = (ListView)findViewById(R.id.idMusicListView);
        mEditPlayer = (EditText)findViewById(R.id.idEditText);

        mMusicListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    SmokeyDB db = SmokeyDB.getInstance();
                    Cursor c = db.getReadableDatabase().rawQuery("SELECT " + SmokeyDB.COL_PARTITION_ID + " FROM " + SmokeyDB.TABLE_PARTITION + " WHERE " + SmokeyDB.COL_PARTITION_NAME + " LIKE \"" + ((TextView) view).getText().toString() + "\";", null);

                    if (c.getCount() == 0) {
                        Log.i("sql", "Music not exists");
                        mMusicIdSelected = -1;
                    }

                    c.moveToFirst();
                    mMusicIdSelected = c.getInt(0);
                    Toast.makeText(getApplicationContext(), "Selected music n°" + mMusicIdSelected, Toast.LENGTH_SHORT).show();

                    c.close();
                } catch (Exception e) {
                    Log.i("sql", e.getMessage());
                    mMusicIdSelected = -1;
                }
            }
        });

        ArrayList<String> musics = new ArrayList<String>();

        // init context for database singleton
        SmokeyDB.setContext(getApplicationContext());
        SmokeyDB db = null;

        try
        {
            db = SmokeyDB.getInstance();
        }
        catch (Exception e)
        {
            Log.i("sql", e.getMessage());
        }

        if (db == null)
        {
            Log.i("sql", "db not initialized");
            return;
        }

        Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM " + SmokeyDB.TABLE_PARTITION + ";", null);

        for (int i = 0; i < c.getCount(); ++i)
        {
            if (!c.moveToPosition(i))
                break;

            musics.add(c.getString(1));
        }

        c.close();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item, musics);
        mMusicListView.setAdapter(adapter);

        mPlayButton = (Button)findViewById(R.id.idPlayButton);
        mScoresButton = (Button)findViewById(R.id.idScoresButton);
        mQuitButton = (Button)findViewById(R.id.idQuitButton);

        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent playIntent = new Intent(ownRef, SmokeyTilesGame.class);
                playIntent.putExtra("PART_ID", mMusicIdSelected);

                int playerId = 0; // default profile

                try {
                    SmokeyDB db = SmokeyDB.getInstance();

                    // lookup for existing player
                    Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM " + SmokeyDB.TABLE_PLAYER + " WHERE " + SmokeyDB.COL_PLAYER_NAME + " LIKE \"" + mEditPlayer.getText() + "\";", null);

                    // player not exists
                    if (c.getCount() == 0) {
                        // get new id

                        Cursor cid = db.getReadableDatabase().rawQuery("SELECT " + SmokeyDB.COL_PLAYER_ID + " FROM " + SmokeyDB.TABLE_PLAYER + " ORDER BY " + SmokeyDB.COL_PLAYER_ID + " DESC LIMIT 1;", null);

                        cid.moveToFirst();
                        int newId = cid.getInt(0);
                        playerId = newId + 1;
                        // register it
                        db.getWritableDatabase().execSQL("INSERT INTO " + SmokeyDB.TABLE_PLAYER + " VALUES(" + playerId + ", \"" + mEditPlayer.getText() + "\");");
                    } else {
                        c.moveToFirst();
                        playerId = c.getInt(0);
                    }
                } catch (Exception e) {
                    Log.i("sql", e.getMessage());
                }

                playIntent.putExtra("PLAY_ID", playerId);
                startActivity(playIntent);
                ownRef.finish();
            }
        });

        mScoresButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scoresIntent = new Intent(ownRef, SmokeyTilesScores.class);
                startActivity(scoresIntent);
                ownRef.finish();
            }
        });

        mQuitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ownRef.finish();
            }
        });
    }



}
