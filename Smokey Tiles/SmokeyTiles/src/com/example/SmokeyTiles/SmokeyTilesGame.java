package com.example.SmokeyTiles;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;

import java.util.*;

public class SmokeyTilesGame extends Activity
{
    private ArrayList<Button> mTiles;
    private ArrayList<SurfaceView> mHints;

    private ArrayList<Note> mCurrentPlay;
    private int mMusicId;
    private int mPlayerID;
    private int mMinFreq, mMaxFreq;
    private boolean mInProgress;
    private int mCurrentScore;
    private int mCurrentTile;

    static int[] ButtonIds = new int[] {R.id.button0, R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5,
            R.id.button6, R.id.button7, R.id.button8, R.id.button9, R.id.button10, R.id.button11};
    static int[] HintIds = new int[] { R.id.surfaceView0, R.id.surfaceView1, R.id.surfaceView2, R.id.surfaceView3, R.id.surfaceView4, R.id.surfaceView5,
            R.id.surfaceView6, R.id.surfaceView7, R.id.surfaceView8, R.id.surfaceView9, R.id.surfaceView10, R.id.surfaceView11 };

    private SmokeyTilesGame ownRef;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);

        ownRef = this;

        mCurrentPlay = new ArrayList<Note>();
        mTiles = new ArrayList<Button>();
        mHints = new ArrayList<SurfaceView>();
        mCurrentScore = 0;
        mInProgress = true;
        mCurrentTile = 0;

        Bundle bundle = getIntent().getExtras();

        mMusicId = bundle.getInt("PART_ID");
        mPlayerID = bundle.getInt("PLAY_ID");

        if (mMusicId == -1)
        {
            FinishActivity("BAD MUSIC SELECTED");
            return;
        }

        Toast.makeText(getApplicationContext(), "mMusicId = " + mMusicId, Toast.LENGTH_SHORT).show();

        for (int i = 0; i < ButtonIds.length; ++i)
        {
            Button b = (Button)findViewById(ButtonIds[i]);
            mTiles.add(b);
            Log.i("game", "b = " + b);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mInProgress && mCurrentTile == (v.getId() - 3) % mTiles.size())
                        addPoint(1);
                }
            });
        }

        for (int i = 0; i < HintIds.length; ++i) {
            SurfaceView sv = (SurfaceView) findViewById(HintIds[i]);
            mHints.add(sv);
        }

        InitGame();
    }

    public void InitGame()
    {
        try {
            SmokeyDB db = SmokeyDB.getInstance();

            //select NOTE.* from NOTE, COMPOSE where idPartCompose == 0 and idNote == idNoteCompose order by orderCompose ASC;
            Cursor c0 = db.getReadableDatabase().rawQuery("select " + SmokeyDB.TABLE_NOTE + ".* from " + SmokeyDB.TABLE_NOTE + ", " + SmokeyDB.TABLE_COMPOSE + " where " + SmokeyDB.COL_COMPOSE_IDPART + " = " + mMusicId + " and " + SmokeyDB.COL_NOTE_ID + " == " + SmokeyDB.COL_COMPOSE_IDNOTE + " order by " + SmokeyDB.COL_COMPOSE_ORDER + " ASC;", null);

            if (c0.getCount() == 0)
            {
                FinishActivity("CANNOT FIND MUSIC DATA");
                return;
            }

            for (int i = 0; i < c0.getCount() && c0.moveToPosition(i); ++i)
            {
                mCurrentPlay.add(new Note(c0.getInt(0), c0.getInt(1), c0.getInt(2), c0.getInt(3)));
            }

            c0.close();

            Cursor c1 = db.getReadableDatabase().rawQuery("select max(" + SmokeyDB.COL_NOTE_FREQ + "), min(" + SmokeyDB.COL_NOTE_FREQ + ") from " + SmokeyDB.TABLE_NOTE + ", " + SmokeyDB.TABLE_COMPOSE + " where " + SmokeyDB.COL_COMPOSE_IDPART + " = " + mMusicId + ";", null);

            if (c1.getCount() == 0)
            {
                FinishActivity("NO FREQUENCY BOUNDS FOUND");
                return;
            }

            c1.moveToFirst();

            mMaxFreq = c1.getInt(0);
            mMinFreq = c1.getInt(1);

            OnGameStarted();
        }
        catch (Exception e)
        {
            Log.i("sql", e.getMessage());
        }
    }

    private void FinishActivity(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        Intent backToMainIntent = new Intent(ownRef, SmokeyTilesMain.class);
        startActivity(backToMainIntent);
        ownRef.finish();
    }

    public void OnGameStarted()
    {
        Log.i("game", "game started");
        new AsyncGameTask().execute();
        addPoint(0);
        Log.i("game", "in progress");
    }

    public void OnGameFinished()
    {
        Log.i("game", "game finished");
        mInProgress = false;

        try
        {
            SmokeyDB db = SmokeyDB.getInstance();
            db.getWritableDatabase().execSQL("INSERT INTO " + SmokeyDB.TABLE_REALIZED + " VALUES(" + mPlayerID + ", " + mMusicId + ", datetime(), " + mCurrentScore + ");");
        }
        catch (Exception e)
        {
            Log.i("sql", e.getMessage());
        }

        OnBackToMain();
    }

    public void SetHintColor(int n, int color)
    {
        final int n_ = n;
        final int color_ = color;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mHints.get(n_).setBackgroundColor(color_);
            }
        });
    }

    public void addPoint(int n)
    {
        this.setTitle("Score : " + (mCurrentScore += n));
    }

    public void OnBackToMain()
    {
        Log.i("game", "on back to main enter");
        final PopupWindow popup = new PopupWindow(this);
        LayoutInflater inf  = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final LinearLayout layout = (LinearLayout)inf.inflate(R.layout.popup, null);
        popup.setContentView(layout);
        layout.post(new Runnable() {
            public void run() {
                popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
                popup.update(300, 150);
                ViewGroup vg = (ViewGroup)layout.findViewById(R.id.idPopupLayout);
                vg.postInvalidate();
            }
        });

        Button btnClose = (Button)layout.findViewById(R.id.idButtonBack);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                Intent backToMainIntent = new Intent(ownRef, SmokeyTilesMain.class);
                startActivity(backToMainIntent);
                ownRef.finish();
            }
        });

        Log.i("game", "on back to main out");
    }

    private class AsyncGameTask extends AsyncTask<Void /*Params*/, Void /*Progress*/, Void/*Result*/>
    {
        private int lastHint = -1;
        @Override
        protected Void doInBackground(Void... params) {

            for (int i = 0; i < mCurrentPlay.size(); ++i)
            {
                try
                {
                    if (lastHint != -1)
                        SmokeyTilesGame.this.SetHintColor(lastHint, 0xFF7F7F7F);

                    Note n = mCurrentPlay.get(i);

                    double p = ((double)((double)(n.getFrequency() - mMinFreq) / (double)(mMaxFreq - mMinFreq)) * (double)100);
                    double pp = p % mHints.size();
                    int h = (int)(pp);
                    SmokeyTilesGame.this.SetHintColor(h, 0xFFFF0000);

                    n.playTone();

                    lastHint = h;
                    mCurrentTile = h;

                    Thread.sleep((long)Math.floor((n.getDuration() + n.getDelay())));
                }
                catch (InterruptedException e)
                {
                    Log.i("async", e.getMessage());
                }
            }

            return null;
        }



        @Override
        protected void onPostExecute(Void result) {
            OnGameFinished();
        }
    }
}