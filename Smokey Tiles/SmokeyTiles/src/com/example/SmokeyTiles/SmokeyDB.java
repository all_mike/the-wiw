package com.example.SmokeyTiles;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by ynden on 18/05/16.
 */

public class SmokeyDB extends SQLiteOpenHelper
{
    private static SmokeyDB mDataBase = null;
    public static Context mContext = null;

    public static final String DB_NAME = "Smokey.db";
    public static final int DB_VERSION = 1;

    public static final String TABLE_PLAYER = "PLAYER";
    public static final String COL_PLAYER_ID = "idPlayer";
    public static final String COL_PLAYER_NAME = "namePlayer";

    public static final String TABLE_PARTITION = "PARTITION";
    public static final String COL_PARTITION_ID = "idPart";
    public static final String COL_PARTITION_NAME = "namePart";

    public static final String TABLE_NOTE = "NOTE";
    public static final String COL_NOTE_ID = "idNote";
    public static final String COL_NOTE_FREQ = "freqNote";
    public static final String COL_NOTE_DUR = "durationNote";
    public static final String COL_NOTE_DELAY = "delayNote";

    public static final String TABLE_COMPOSE = "COMPOSE";
    public static final String COL_COMPOSE_IDNOTE = "idNoteCompose";
    public static final String COL_COMPOSE_IDPART = "idPartCompose";
    public static final String COL_COMPOSE_ORDER = "orderCompose";

    public static final String TABLE_REALIZED = "REALIZED";
    public static final String COL_REALIZED_IDPART = "idPartRealized";
    public static final String COL_REALIZED_IDPLAY = "idPlayRealized";
    public static final String COL_REALIZED_DATE = "dateRealized";
    public static final String COL_REALIZED_SCORE = "scoreRealized";

    /*
        CREATE TABLE TABLE_COMPOSE (
          COL_COMPOSE_IDNOTE INTEGER,
          COL_COMPOSE_IDPART INTEGER,
          COL_COMPOSE_ORDER INTEGER,
          PRIMARY KEY (COL_COMPOSE_IDNOTE, COL_COMPOSE_IDPART),
          FOREIGN KEY (COL_COMPOSE_IDNOTE) REFERENCES TABLE_NOTE (COL_NOTE_ID),
          FOREIGN KEY (COL_COMPOSE_IDPART) REFERENCES TABLE_PARTITION (COL_PARTITION_ID)
        );

        CREATE TABLE TABLE_PARTITION (
          COL_PARTITION_ID INTEGER,
          COL_PARTITION_NAME VARCHAR(42),
          PRIMARY KEY (COL_PARTITION_ID)
        );

        CREATE TABLE TABLE_REALIZED (
          COL_REALIZED_IDPLAY INTEGER,
          COL_REALIZED_IDPART INTEGER,
          COL_REALIZED_DATE DATE,
          COL_REALIZED_SCORE INTEGER,
          PRIMARY KEY (COL_REALIZED_IDPLAY, COL_REALIZED_IDPART, COL_REALIZED_DATE),
          FOREIGN KEY (COL_REALIZED_IDPLAY) REFERENCES TABLE_PLAYER (COL_PLAYER_ID),
          FOREIGN KEY (COL_REALIZED_IDPART) REFERENCES TABLE_PARTITION (COL_PARTITION_ID)
        );

        CREATE TABLE TABLE_NOTE (
          COL_NOTE_ID INTEGER,
          COL_NOTE_FREQ INTEGER,
          COL_NOTE_DUR INTEGER,
          COL_NOTE_DELAY INTEGER,
          PRIMARY KEY (COL_NOTE_ID)
        );

        CREATE TABLE TABLE_PLAYER (
          COL_PLAYER_ID INTEGER,
          COL_PLAYER_NAME VARCHAR(42),
          PRIMARY KEY (COL_PLAYER_ID)
        );
    */
    public static final String CREATE_BDD_PART = "CREATE TABLE IF NOT EXISTS " + TABLE_PARTITION + "(\n" +
            "" + COL_PARTITION_ID + " INTEGER,\n" +
            "" + COL_PARTITION_NAME + " VARCHAR(42),\n" +
            "PRIMARY KEY (" + COL_PARTITION_ID + ")\n" +
            ");";


    public static final String CREATE_BDD_NOTE = "CREATE TABLE IF NOT EXISTS " + TABLE_NOTE + "(\n" +
            "" + COL_NOTE_ID + " INTEGER,\n" +
            "" + COL_NOTE_FREQ + " INTEGER,\n" +
            "" + COL_NOTE_DUR + " INTEGER,\n" +
            "" + COL_NOTE_DELAY + " INTEGER,\n" +
            "PRIMARY KEY (" + COL_NOTE_ID + ")\n" +
            ");";

    public static final String CREATE_BDD_PLAY = "CREATE TABLE IF NOT EXISTS " + TABLE_PLAYER + "(\n" +
            "" + COL_PLAYER_ID + " INTEGER,\n" +
            "" + COL_PLAYER_NAME + " VARCHAR(42),\n" +
            "PRIMARY KEY (" + COL_PLAYER_ID + ")\n" +
            ");";

    public static final String CREATE_BDD_COMP = "CREATE TABLE IF NOT EXISTS " + TABLE_COMPOSE + "(\n" +
            "" + COL_COMPOSE_IDNOTE + " INTEGER,\n" +
            "" + COL_COMPOSE_IDPART + " INTEGER,\n" +
            "" + COL_COMPOSE_ORDER + " INTEGER,\n" +
            "PRIMARY KEY (" + COL_COMPOSE_IDNOTE +", " + COL_COMPOSE_IDPART + ", " + COL_COMPOSE_ORDER + "),\n" +
            "FOREIGN KEY (" + COL_COMPOSE_IDNOTE + ") REFERENCES " + TABLE_NOTE + "("+ COL_NOTE_ID +"),\n" +
            "FOREIGN KEY (" + COL_COMPOSE_IDPART + ") REFERENCES " + TABLE_PARTITION + "(" + COL_PARTITION_ID+ ")\n" +
            ");";

    public static final String CREATE_BDD_REAL = "CREATE TABLE IF NOT EXISTS " + TABLE_REALIZED + "(\n" +
            "" + COL_REALIZED_IDPLAY + " INTEGER,\n" +
            "" + COL_REALIZED_IDPART + " INTEGER,\n" +
            "" + COL_REALIZED_DATE + " DATE,\n" +
            "" + COL_REALIZED_SCORE + " INTEGER,\n" +
            "PRIMARY KEY (" + COL_REALIZED_IDPLAY + ", " + COL_REALIZED_IDPART + ", " + COL_REALIZED_DATE + "),\n" +
            "FOREIGN KEY (" + COL_REALIZED_IDPLAY + ") REFERENCES " + TABLE_PLAYER +  "(" + COL_PLAYER_ID + "),\n" +
            "FOREIGN KEY (" + COL_REALIZED_IDPART + ") REFERENCES " + TABLE_PARTITION + "(" + COL_PARTITION_ID + ")\n" +
            ");";

    public static final String CREATE_BDD = CREATE_BDD_PART + "\n" +
                                            CREATE_BDD_NOTE + "\n" +
                                            CREATE_BDD_PLAY + "\n" +
                                            CREATE_BDD_COMP + "\n" +
                                            CREATE_BDD_REAL;

    /*public static final String[] Mario = {
            "INSERT INTO PARTITION(idPart, namePart) VALUES (0, \"Mario Theme\");",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (0, 330, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 1);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 2);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (1, 262, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 3);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 4);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (2, 392, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 5);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (3, 196, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (3, 0, 6);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (4, 262, 300, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (4, 0, 7);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (5, 196, 300, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (5, 0, 8);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (6, 164, 300, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (6, 0, 9);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (7, 220, 300, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (7, 0, 10);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (8, 246, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (8, 0, 11);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (9, 233, 200, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (9, 0, 12);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (10, 220, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (10, 0, 13);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (3, 0, 14);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 15);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 16);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (11, 440, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (11, 0, 17);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (12, 349, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (12, 0, 18);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 19);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 20);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 21);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (13, 294, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (13, 0, 22);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (14, 247, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (14, 0, 23);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (4, 0, 24);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (5, 0, 25);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (6, 0, 26);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (7, 0, 27);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (8, 0, 28);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (9, 0, 29);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (10, 0, 30);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (3, 0, 31);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 32);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 33);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (11, 0, 34);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (12, 0, 35);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 36);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 37);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 38);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (13, 0, 39);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (14, 0, 40);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 41);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (15, 370, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (15, 0, 42);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (12, 0, 43);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (16, 311, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (16, 0, 44);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 45);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (17, 207, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (17, 0, 46);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (10, 0, 47);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 48);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (10, 0, 49);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 50);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (13, 0, 51);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 52);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (15, 0, 53);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (12, 0, 54);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (16, 0, 55);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 56);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (18, 523, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (18, 0, 57);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (18, 0, 58);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (18, 0, 59);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 60);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (15, 0, 61);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (12, 0, 62);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (16, 0, 63);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 64);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (17, 0, 65);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (10, 0, 66);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 67);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (10, 0, 68);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 69);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (13, 0, 70);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (19, 311, 300, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (19, 0, 71);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (20, 296, 300, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (20, 0, 72);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (4, 0, 73);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 74);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 75);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 76);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 77);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (13, 0, 78);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (21, 330, 200, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (21, 0, 79);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (22, 262, 200, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (22, 0, 80);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (23, 220, 200, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (23, 0, 81);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (3, 0, 82);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 83);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 84);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 85);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 86);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (13, 0, 87);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 88);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (11, 0, 89);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 90);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 91);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 92);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 93);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 94);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (13, 0, 95);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (21, 0, 96);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (22, 0, 97);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (23, 0, 98);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (3, 0, 99);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 100);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 101);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 102);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 103);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 104);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 105);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (3, 0, 106);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (3, 0, 107);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 108);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 109);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 110);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (18, 0, 111);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (24, 660, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (24, 0, 112);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (25, 784, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (25, 0, 113);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (24, 0, 114);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (17, 0, 115);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 116);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (16, 0, 117);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (26, 415, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (26, 0, 118);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (18, 0, 119);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (27, 622, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (27, 0, 120);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (28, 830, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (28, 0, 121);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (27, 0, 122);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (29, 233, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (29, 0, 123);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (13, 0, 124);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (12, 0, 125);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (30, 466, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (30, 0, 126);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (31, 587, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (31, 0, 127);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (32, 698, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (32, 0, 128);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (33, 932, 100, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (33, 0, 129);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (33, 0, 130);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (33, 0, 131);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (33, 0, 132);",
            "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (34, 1046, 675, 0);",
            "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (34, 0, 133);"
    };*/
    
    public static final String[] Mario = {
            "INSERT INTO PARTITION(idPart, namePart) VALUES (0, \"Mario Theme\");",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (0, 330, 100, 100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 0);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (1, 330, 100, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 1);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 2);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (2, 262, 100, 100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 3);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 4);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (3, 392, 100, 700);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (3, 0, 5);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (4, 196, 100, 700);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (4, 0, 6);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (5, 262, 300, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (5, 0, 7);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (6, 196, 300, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (6, 0, 8);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (7, 164, 300, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (7, 0, 9);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (8, 220, 300, 100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (8, 0, 10);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (9, 246, 100, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (9, 0, 11);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (10, 233, 200, 0);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (10, 0, 12);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (11, 220, 100, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (11, 0, 13);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (12, 196, 100, 150);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (12, 0, 14);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (13, 330, 100, 150);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (13, 0, 15);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (14, 392, 100, 150);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (14, 0, 16);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (15, 440, 100, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (15, 0, 17);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (16, 349, 100, 100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (16, 0, 18);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (17, 392, 100, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (17, 0, 19);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 20);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 21);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (18, 294, 100, 100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (18, 0, 22);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (19, 247, 100, 500);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (19, 0, 23);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (5, 0, 24);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (6, 0, 25);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (7, 0, 26);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (8, 0, 27);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (9, 0, 28);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (10, 0, 29);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (11, 0, 30);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (12, 0, 31);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (13, 0, 32);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (14, 0, 33);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (15, 0, 34);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (16, 0, 35);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (17, 0, 36);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 37);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 38);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (18, 0, 39);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (20, 247, 100, 900);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (20, 0, 40);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (21, 392, 100, 100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (21, 0, 41);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (22, 370, 100, 100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (22, 0, 42);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (16, 0, 43);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (23, 311, 100, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (23, 0, 44);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 45);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (24, 207, 100, 100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (24, 0, 46);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (25, 220, 100, 100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (25, 0, 47);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (26, 262, 100, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (26, 0, 48);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (25, 0, 49);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 50);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (27, 294, 100, 500);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (27, 0, 51);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (21, 0, 52);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (22, 0, 53);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (16, 0, 54);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (23, 0, 55);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 56);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (28, 523, 100, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (28, 0, 57);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (29, 523, 100, 100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (29, 0, 58);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (30, 523, 100, 1100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (30, 0, 59);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (21, 0, 60);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (22, 0, 61);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (16, 0, 62);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (23, 0, 63);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 64);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (24, 0, 65);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (25, 0, 66);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (26, 0, 67);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (25, 0, 68);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 69);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (27, 0, 70);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (31, 311, 300, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (31, 0, 71);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (32, 296, 300, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (32, 0, 72);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (33, 262, 300, 1300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (33, 0, 73);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 74);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (26, 0, 75);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (26, 0, 76);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 77);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (34, 294, 100, 300);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (34, 0, 78);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (35, 330, 200, 50);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (35, 0, 79);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (36, 262, 200, 50);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (36, 0, 80);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (37, 220, 200, 50);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (37, 0, 81);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (4, 0, 82);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 83);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (26, 0, 84);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (26, 0, 85);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 86);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (18, 0, 87);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (38, 330, 100, 700);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (38, 0, 88);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (15, 0, 89);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (39, 392, 100, 500);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (39, 0, 90);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 91);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (26, 0, 92);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (26, 0, 93);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 94);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (34, 0, 95);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (35, 0, 96);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (36, 0, 97);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (37, 0, 98);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (4, 0, 99);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (0, 0, 100);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 101);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 102);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (2, 0, 103);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (1, 0, 104);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (3, 0, 105);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (4, 0, 106);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (40, 196, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (40, 0, 107);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (41, 262, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (41, 0, 108);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (42, 330, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (42, 0, 109);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (43, 392, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (43, 0, 110);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (44, 523, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (44, 0, 111);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (45, 660, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (45, 0, 112);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (46, 784, 100, 575);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (46, 0, 113);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (47, 660, 100, 575);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (47, 0, 114);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (48, 207, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (48, 0, 115);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (41, 0, 116);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (49, 311, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (49, 0, 117);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (50, 415, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (50, 0, 118);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (44, 0, 119);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (51, 622, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (51, 0, 120);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (52, 830, 100, 575);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (52, 0, 121);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (53, 622, 100, 575);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (53, 0, 122);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (54, 233, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (54, 0, 123);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (55, 294, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (55, 0, 124);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (56, 349, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (56, 0, 125);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (57, 466, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (57, 0, 126);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (58, 587, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (58, 0, 127);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (59, 698, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (59, 0, 128);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (60, 932, 100, 575);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (60, 0, 129);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (61, 932, 100, 125);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (61, 0, 130);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (61, 0, 131);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (61, 0, 132);",
    "INSERT INTO NOTE(idNote, freqNote, durationNote, delayNote) VALUES (62, 1046, 675, 0);",
    "INSERT INTO COMPOSE(idNoteCompose, idPartCompose, orderCompose) VALUES (62, 0, 133);",

};

    public SmokeyDB(Context context, String name, CursorFactory cursFactory, int version)
    {
        super(context, name, cursFactory, version);
        onCreate(this.getWritableDatabase());
        Log.i("sql", "ctor");
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CREATE_BDD_PLAY);
        db.execSQL(CREATE_BDD_NOTE);
        db.execSQL(CREATE_BDD_PART);
        db.execSQL(CREATE_BDD_COMP);
        db.execSQL(CREATE_BDD_REAL);

        Cursor c = db.rawQuery("SELECT * FROM " + SmokeyDB.TABLE_PARTITION + ";", null);

        if (c.getCount() == 0) {
            // init default music
            for (String req : Mario)
                db.execSQL(req);
        }

        c.close();

        // default player
        c = db.rawQuery("SELECT * FROM " + SmokeyDB.TABLE_PLAYER + " WHERE " + SmokeyDB.COL_PLAYER_ID + " = 0;", null);

        if (c.getCount() == 0)
            db.execSQL("INSERT INTO PLAYER VALUES (" + 0 + ", \"PLAYER_1\");");

        Log.i("sql", "on_create");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REALIZED);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMPOSE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARTITION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTE);

        onCreate(db);


        Log.i("sql", "on_upgrade");
    }

    public static void setContext(Context ctx)
    {
        mContext = ctx;
    }

    public static Context getContext()
    {
        return mContext;
    }

    public static SmokeyDB getInstance() throws Exception
    {
        if (getContext() == null)
            throw new Exception("Set context before !");

        if (mDataBase == null)
            mDataBase = new SmokeyDB(getContext(), DB_NAME, null, DB_VERSION);

        return mDataBase;
    }
}
