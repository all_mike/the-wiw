package com.sncf.igtl.re.sicm.android;

import android.app.Application;
import android.util.Log;

import com.sncf.igtl.re.sicm.android.network.SicmService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SicmApp extends Application {

    private SicmService sicmService;

    @Override
    public void onCreate() {
        super.onCreate();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                System.out.println("---------------------------------------------------------------");
                Log.i("OK HTTP", message);
            }
        });
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        // TODO Retrieve host and path by configuration

        // Borne
        /*
        Retrofit r = new Retrofit.Builder()
                .baseUrl("http://s50p12en511:8080/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        */

        // PC Portable

        Retrofit r = new Retrofit.Builder()
                .baseUrl("http://192.168.43.184:8080/api/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.sicmService = r.create(SicmService.class);

    }

    public SicmService getSicmService() {
        return this.sicmService;
    }
}