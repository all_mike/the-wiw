package com.sncf.igtl.re.sicm.android.activity.voies;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.EntypoModule;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.joanzapata.iconify.fonts.IoniconsModule;
import com.joanzapata.iconify.fonts.MaterialCommunityModule;
import com.joanzapata.iconify.fonts.MaterialModule;
import com.joanzapata.iconify.fonts.MeteoconsModule;
import com.joanzapata.iconify.fonts.SimpleLineIconsModule;
import com.joanzapata.iconify.fonts.TypiconsModule;
import com.joanzapata.iconify.fonts.WeathericonsModule;
import com.sncf.igtl.re.sicm.android.R;
import com.sncf.igtl.re.sicm.android.SicmApp;
import com.sncf.igtl.re.sicm.android.model_retrofit.Track;
import com.sncf.igtl.re.sicm.android.model_retrofit.TrackWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.Workshop;
import com.sncf.igtl.re.sicm.android.model_retrofit.WorkshopWrapper;
import com.sncf.igtl.re.sicm.android.network.UniqueResourceRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ChoixVoies extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Iconify
                .with(new FontAwesomeModule())
                .with(new EntypoModule())
                .with(new TypiconsModule())
                .with(new MaterialModule())
                .with(new MaterialCommunityModule())
                .with(new MeteoconsModule())
                .with(new WeathericonsModule())
                .with(new SimpleLineIconsModule())
                .with(new IoniconsModule());
        setContentView(R.layout.activity_choix_voies);

        ListView lv = (ListView) findViewById(R.id.ListView_choixVoies);

        SicmApp app = ((SicmApp) getApplicationContext());
        // TODO Retrieve workshop ID by configuration
        try {
            final Workshop workshop = new UniqueResourceRequest<WorkshopWrapper>(app.getSicmService())
                    .execute(app.getSicmService().getWorkshop(1))
                    .get().getWorkshop();
            final int[] tracksIds = workshop.getTracks();
            final List<Track> tracks = new ArrayList<>(tracksIds.length);
            for (int trackId : tracksIds) {
                tracks.add(new UniqueResourceRequest<TrackWrapper>(app.getSicmService())
                        .execute(app.getSicmService().getTrack(trackId))
                        .get().getTrack());
            }
            ListAdapter customAdapter = new AdapterVoies(this, R.layout.voie, tracks);
            lv.setAdapter(customAdapter);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
