package com.sncf.igtl.re.sicm.android.model_retrofit;

public class Slot {
    private int id;
    private int track;
    private String position;
    private int carriage;

    public Slot(int id, int track, String position, int carriage) {
        this.id = id;
        this.track = track;
        this.position = position;
        this.carriage = carriage;
    }

    public int getId() {
        return id;
    }

    public int getTrack() {
        return track;
    }

    public String getPosition() {
        return position;
    }

    public int getCarriage() {
        return carriage;
    }

    public String toString() {
        return "Slot{" +
                "id=" + id +
                ", track=" + track +
                ", position='" + position + '\'' +
                ", carriage=" + carriage +
                '}';
    }
}