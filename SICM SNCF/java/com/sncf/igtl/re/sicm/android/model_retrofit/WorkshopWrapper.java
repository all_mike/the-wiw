package com.sncf.igtl.re.sicm.android.model_retrofit;

public class WorkshopWrapper {

    private final Workshop workshop;

    public WorkshopWrapper(Workshop workshop) {
        this.workshop = workshop;
    }

    public Workshop getWorkshop() {
        return this.workshop;
    }
}
