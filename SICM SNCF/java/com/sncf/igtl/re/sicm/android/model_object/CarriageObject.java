package com.sncf.igtl.re.sicm.android.model_object;

import java.util.ArrayList;

public class CarriageObject {
    private ArrayList<TaskObject> tasks;

    public CarriageObject() {
        this.tasks = new ArrayList<TaskObject>();
    }

    public ArrayList<TaskObject> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<TaskObject> tasks) {
        this.tasks = tasks;
    }
}
