package com.sncf.igtl.re.sicm.android.activity.taches;

import com.sncf.igtl.re.sicm.android.model_retrofit.Operation;
import com.sncf.igtl.re.sicm.android.model_retrofit.Task;

public class TaskOP {
    private Task task;
    private Operation operation;
    private String position;
    private boolean isPending;

    public TaskOP(Task task, Operation operation, String position, boolean isPending) {
        this.task = task;
        this.operation = operation;
        this.position = position;
        this.isPending = isPending;
    }

    public Task getTask() {
        return task;
    }

    public Operation getOperation() {
        return operation;
    }

    public String getPosition(){
        return this.position;
    }

    public boolean isPending() {
        return isPending;
    }
}
