package com.sncf.igtl.re.sicm.android.activity;

import com.sncf.igtl.re.sicm.android.SicmApp;
import com.sncf.igtl.re.sicm.android.activity.taches.TaskOP;
import com.sncf.igtl.re.sicm.android.model_retrofit.Carriage;
import com.sncf.igtl.re.sicm.android.model_retrofit.CarriageWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.CoverageWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.Operation;
import com.sncf.igtl.re.sicm.android.model_retrofit.OperationWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.Slot;
import com.sncf.igtl.re.sicm.android.model_retrofit.SlotWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.Task;
import com.sncf.igtl.re.sicm.android.model_retrofit.TaskWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.Track;
import com.sncf.igtl.re.sicm.android.network.UniqueResourceRequest;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class ListeOperations {
    public static ArrayList<TaskOP> getOP(SicmApp app, Track track) {

        ArrayList<Slot> listSlots = new ArrayList<>();
        ArrayList<Carriage> listCarriages = new ArrayList<>();
        ArrayList<TaskOP> listTaskOP = new ArrayList<>();
        try {
            for(int i : track.getSlots()){
                listSlots.add(
                        new UniqueResourceRequest<SlotWrapper>(app.getSicmService())
                                .execute(app.getSicmService().getSlot(i))
                                .get().getSlot());
            }
            for (Slot s : listSlots){
                if(new UniqueResourceRequest<CarriageWrapper>(app.getSicmService())
                        .execute(app.getSicmService().getCarriage(s.getCarriage()))
                        .get() != null){
                    listCarriages.add(
                            new UniqueResourceRequest<CarriageWrapper>(app.getSicmService())
                                    .execute(app.getSicmService().getCarriage(s.getCarriage()))
                                    .get()
                                    .getCarriage()
                    );
                }
            }
            for(Carriage c : listCarriages) {
                for(int t : c.getTasks()) {
                    Task task =
                            new UniqueResourceRequest<TaskWrapper>(app.getSicmService())
                            .execute(app.getSicmService().getTask(t))
                            .get()
                            .getTask();
                    Operation operation =
                            new UniqueResourceRequest<OperationWrapper>(app.getSicmService())
                            .execute(app.getSicmService().getOperation(task.getOperation()))
                            .get()
                            .getOperation();
                    String position = c.getPosition();
                    boolean isPending = false;
                    for(int j : task.getCoverages()){
                        if(new UniqueResourceRequest<CoverageWrapper>(app.getSicmService())
                                .execute(app.getSicmService().getCoverage(j))
                                .get()
                                .getCoverage();
                    }
                    int[] coverages = new UniqueResourceRequest<CoverageWrapper>(app.getSicmService())
                            .execute(app.getSicmService().getCoverage(task.getCoverages()))
                            .get()
                            .getCoverage();
                    listTaskOP.add(new TaskOP(task,operation,position,isPending));
                }
            }
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e1) {
            e1.printStackTrace();
        }
        return listTaskOP;
    }
}
