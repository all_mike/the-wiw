package com.sncf.igtl.re.sicm.android.model_retrofit;

public class SlotWrapper {
    private Slot slot;

    public SlotWrapper(Slot slot) {
        this.slot = slot;
    }

    public Slot getSlot() {
        return this.slot;
    }
}
