package com.sncf.igtl.re.sicm.android.model_retrofit;

/**
 * Created by SMML05901 on 22/08/2016.
 */
public class CoverageWrapper {

    private final Coverage coverage;

    public CoverageWrapper(Coverage coverage) {
        this.coverage = coverage;
    }

    public Coverage getCoverage(){
        return this.coverage;
    }
}
