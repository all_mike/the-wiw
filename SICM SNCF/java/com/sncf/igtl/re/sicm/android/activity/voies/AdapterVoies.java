package com.sncf.igtl.re.sicm.android.activity.voies;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.EntypoModule;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.joanzapata.iconify.fonts.IoniconsModule;
import com.joanzapata.iconify.fonts.MaterialCommunityIcons;
import com.joanzapata.iconify.fonts.MaterialCommunityModule;
import com.joanzapata.iconify.fonts.MaterialModule;
import com.joanzapata.iconify.fonts.MeteoconsModule;
import com.joanzapata.iconify.fonts.SimpleLineIconsModule;
import com.joanzapata.iconify.fonts.TypiconsModule;
import com.joanzapata.iconify.fonts.WeathericonsModule;
import com.sncf.igtl.re.sicm.android.R;
import com.sncf.igtl.re.sicm.android.activity.taches.ChoixTaches;
import com.sncf.igtl.re.sicm.android.model_retrofit.Track;

import java.util.List;

public class AdapterVoies extends ArrayAdapter<Track> {
    public AdapterVoies(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }
    public AdapterVoies(Context context, int resource, List<Track> items) {
        super(context, resource, items);
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        Iconify
                .with(new FontAwesomeModule())
                .with(new EntypoModule())
                .with(new TypiconsModule())
                .with(new MaterialModule())
                .with(new MaterialCommunityModule())
                .with(new MeteoconsModule())
                .with(new WeathericonsModule())
                .with(new SimpleLineIconsModule())
                .with(new IoniconsModule());
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.voie, null);
        }

        final Track map = getItem(position);

        if (map != null) {

            TextView tv1 = (TextView) v.findViewById(R.id.TextView_Item_ListView_ChoixVoies);
            TextView tv2 = (TextView) v.findViewById(R.id.mecouvrir_textview);
            ImageView iv1 = (ImageView) v.findViewById(R.id.is_protected);
            ImageView iv2 = (ImageView) v.findViewById(R.id.is_under_power);
            LinearLayout l1 = (LinearLayout) v.findViewById(R.id.linear_voie);
            LinearLayout l2 = (LinearLayout) v.findViewById(R.id.mecouvrir_layout);

            if (tv1 != null) {
                tv1.setText("Voie n°"+map.getNumber());
            }
            if (iv2 != null) {
                if (map.isUnderPower()){
                    iv2.setImageDrawable(new IconDrawable(getContext(), MaterialCommunityIcons.mdi_flash));
                } else {
                    iv2.setImageDrawable(new IconDrawable(getContext(), MaterialCommunityIcons.mdi_flash_off));
                }
            }
            if (iv1 != null) {
                if (map.isProtected()) {
                    l2.setBackgroundColor(Color.parseColor("#D81B61"));
                    l2.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Intent i = new Intent(getContext(),ChoixTaches.class);
                            i.putExtra("com.sncf.igtl.re.sicm.android.model.TRACK", map);
                            getContext().startActivity(i);
                        }
                    });
                    l1.setBackgroundColor(Color.parseColor("#FBFBFB"));
                    iv2.setBackgroundColor(Color.YELLOW);
                    tv1.setTextColor(Color.BLACK);
                    tv2.setTextColor(Color.WHITE);
                    iv1.setImageDrawable(new IconDrawable(getContext(), FontAwesomeIcons.fa_check_circle));
                    iv1.setBackgroundColor(Color.GREEN);
                } else {
                    iv1.setImageDrawable(new IconDrawable(getContext(), FontAwesomeIcons.fa_minus_circle));

                }
            }
        }
        return v;
    }
}
