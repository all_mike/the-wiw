package com.sncf.igtl.re.sicm.android.model_retrofit;

import java.util.Arrays;

public class Carriage {
    private int id;
    private String number;
    private String position;
    private int train;
    private String schedule;
    private int slot;
    private int[] tasks;
    private int workshop;

    public Carriage(int id, String number, String position, int train, String schedule, int slot, int[] tasks, int workshop) {
        this.id = id;
        this.number = number;
        this.position = position;
        this.train = train;
        this.schedule = schedule;
        this.slot = slot;
        this.tasks = tasks;
        this.workshop = workshop;
    }

    public int getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public String getPosition() {
        return position;
    }

    public int getTrain() {
        return train;
    }

    public String getSchedule() {
        return schedule;
    }

    public int getSlot() {
        return slot;
    }

    public int[] getTasks() {
        return tasks;
    }

    public int getWorkshop() {
        return workshop;
    }

    public String toString() {
        return "Carriage{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", position=" + position +
                ", train=" + train +
                ", schedule=" + schedule +
                ", slot=" + slot +
                ", tasks=" + Arrays.toString(tasks) +
                ", workshop=" + workshop +
                '}';
    }
}