package com.sncf.igtl.re.sicm.android.model_retrofit;

import java.util.Date;

public class Task {
    private int id;
    private Date completeDateTime;
    private String status;
    private int operation;
    private int carriage;
    private int[] coverages;

    public Task(int id, Date completeDateTime, String status, int operation, int carriage, int[] coverages) {
        this.id = id;
        this.completeDateTime = completeDateTime;
        this.status = status;
        this.operation = operation;
        this.carriage = carriage;
        this.coverages = coverages;
    }

    public int getId() {
        return id;
    }

    public Date getCompleteDateTime() {
        return completeDateTime;
    }

    public String getStatus() {
        return status;
    }

    public int getOperation() {
        return operation;
    }

    public int getCarriage() {
        return carriage;
    }

    public int[] getCoverages() {
        return coverages;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}