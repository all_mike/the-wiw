package com.sncf.igtl.re.sicm.android.model_retrofit;

public class TaskWrapper {
    private Task task;

    public TaskWrapper(Task task) {
        this.task = task;
    }

    public Task getTask() {
        return this.task;
    }
}
