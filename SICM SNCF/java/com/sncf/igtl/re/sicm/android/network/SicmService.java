package com.sncf.igtl.re.sicm.android.network;

import com.sncf.igtl.re.sicm.android.model_retrofit.CarriageWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.CoverageWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.OperationWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.SlotWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.TaskWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.TrackWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.WorkshopWrapper;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SicmService {

    @GET("workshops/{id}")
    Call<WorkshopWrapper> getWorkshop(@Path("id") int id);

    @GET("tracks/{id}")
    Call<TrackWrapper> getTrack(@Path("id") int id);

    @GET("carriages/{id}")
    Call<CarriageWrapper> getCarriage(@Path("id") int id);

    @GET("operations/{id}")
    Call<OperationWrapper> getOperation(@Path("id") int id);

    @GET("slots/{id}")
    Call<SlotWrapper> getSlot(@Path("id") int id);

    @GET("tasks/{id}")
    Call<TaskWrapper> getTask(@Path("id") int id);

    @GET("coverages/{id}")
    Call<TaskWrapper> getCoverage(@Path("id") int id);

    @POST("coverages")
    Call<CoverageWrapper> createCoverage(@Body CoverageWrapper coverage);

    @DELETE("coverages/{id}")
    Call<CoverageWrapper> deleteCoverage(@Path("id") int coverageId);

}