package com.sncf.igtl.re.sicm.android.model_retrofit;

import java.util.Arrays;

public class Operation {
    private int id;
    private String name;
    private String description;
    private Integer[] tasks;
    private int category;
    private Integer[] workshops;

    public Operation(int id, String name, String description, Integer[] tasks, int category, Integer[] workshops) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.tasks = tasks;
        this.category = category;
        this.workshops = workshops;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer[] getTasks() {
        return tasks;
    }

    public int getCategory() {
        return category;
    }

    public Integer[] getWorkshops() {
        return workshops;
    }

    public String toString() {
        return "Operation{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", tasks=" + Arrays.toString(tasks) +
                ", category=" + category +
                ", workshops=" + Arrays.toString(workshops) +
                '}';
    }
}