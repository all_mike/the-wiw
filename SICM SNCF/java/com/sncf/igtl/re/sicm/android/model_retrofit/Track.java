package com.sncf.igtl.re.sicm.android.model_retrofit;

import android.os.Parcel;
import android.os.Parcelable;

public class Track implements Parcelable {

    private int id;
    private int number;
    private boolean isProtected;
    private boolean isUnderPower;
    private int[] slots;
    private int workshop;

    // CONSTRUCTEURS

    public Track(int id, int number, boolean isProtected, boolean isUnderPower, int[] slots, int workshop) {
        this.id = id;
        this.number = number;
        this.isProtected = isProtected;
        this.isUnderPower = isUnderPower;
        this.slots = slots;
        this.workshop = workshop;
    }

    // GETTERS

    public int getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public boolean isProtected() {
        return isProtected;
    }

    public boolean isUnderPower() {
        return isUnderPower;
    }

    public int[] getSlots() {
        return slots;
    }

    public int getSlots(int i) {
        return slots[i];
    }

    public int getWorkshop(){
        return workshop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int i) {
        p.writeInt(this.id);
        p.writeInt(this.number);
        p.writeInt((this.isProtected) ? 1 : 0);
        p.writeInt((this.isUnderPower) ? 1 : 0);
        p.writeIntArray(this.slots);
        p.writeInt(this.workshop);
    }

    public static final Parcelable.Creator<Track> CREATOR = new Parcelable.Creator<Track>() {
        @Override
        public Track createFromParcel(Parcel source) {
            return new Track(source);
        }

        @Override
        public Track[] newArray(int size) {
            return new Track[size];
        }
    };

    public Track(Parcel in) {
        id = in.readInt();
        number = in.readInt();
        isProtected = in.readInt() == 1;
        isUnderPower = in.readInt() == 1;
        slots = in.createIntArray();
        workshop = in.readInt();
    }

}