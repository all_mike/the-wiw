package com.sncf.igtl.re.sicm.android.activity.taches;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.sncf.igtl.re.sicm.android.R;

import java.util.List;

public class AdapterTaches extends ArrayAdapter<TaskOP> {

    public AdapterTaches(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }
    public AdapterTaches(Context context, int resource, List<TaskOP> items) {
        super(context, resource, items);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Iconify.with(new FontAwesomeModule());
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.slide_row, null);
        }

        final TaskOP map = getItem(position);

        if (map != null) {

            LinearLayout ll = (LinearLayout) v.findViewById(R.id.Linear_slide_row);
            ImageView iv = (ImageView) v.findViewById(R.id.ImageView_slide_row);
            TextView tv = (TextView) v.findViewById(R.id.TextView_slide_row);
            final LinearLayout border = (LinearLayout) v.findViewById(R.id.border);

            if (iv != null) {
                iv.setImageDrawable(new IconDrawable(getContext(), FontAwesomeIcons.fa_pause_circle));
                if(map.getTask().getStatus().equals("IN_PROGRESS")){
                    ll.setBackgroundColor(Color.parseColor("#FFA500"));
                } else {
                    if(map.getTask().getStatus().equals("FINISHED")){
                        ll.setBackgroundColor(Color.parseColor("#2196F3"));
                    }
                }
            }

            if (tv != null) {
                tv.setText(map.getOperation().getName());
            }
        }
        return v;
    }
}
