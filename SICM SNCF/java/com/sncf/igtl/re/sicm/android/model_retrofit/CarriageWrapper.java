package com.sncf.igtl.re.sicm.android.model_retrofit;

public class CarriageWrapper {
    private Carriage carriage;

    public CarriageWrapper(Carriage carriage) {
        this.carriage = carriage;
    }

    public Carriage getCarriage() {
        return this.carriage;
    }
}
