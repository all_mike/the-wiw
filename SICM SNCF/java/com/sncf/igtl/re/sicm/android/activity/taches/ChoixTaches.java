package com.sncf.igtl.re.sicm.android.activity.taches;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.sncf.igtl.re.sicm.android.R;
import com.sncf.igtl.re.sicm.android.SicmApp;
import com.sncf.igtl.re.sicm.android.activity.ListeOperations;
import com.sncf.igtl.re.sicm.android.activity.voies.ChoixVoies;
import com.sncf.igtl.re.sicm.android.model_retrofit.Coverage;
import com.sncf.igtl.re.sicm.android.model_retrofit.CoverageWrapper;
import com.sncf.igtl.re.sicm.android.model_retrofit.Track;
import com.sncf.igtl.re.sicm.android.network.UniqueResourceRequest;

import java.util.ArrayList;
import java.util.Vector;

public class ChoixTaches extends FragmentActivity {

    final ArrayList<TaskOP> taskChoisies = new ArrayList<>();
    final ArrayList<TaskOP> front = new ArrayList<>();
    final ArrayList<TaskOP> rear = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_taches);

        final SicmApp app = ((SicmApp) getApplicationContext());

        TextView tv = (TextView) findViewById(R.id.TextView1_ChoixActivites);
        TextView send = (TextView) findViewById(R.id.send);

        final Intent i = getIntent();
        Track t = i.getParcelableExtra("com.sncf.igtl.re.sicm.android.model.TRACK");

        tv.setText("Voie n°"+t.getNumber());

        send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                for(TaskOP t : taskChoisies){
                    Log.d("Choix des taches", String.valueOf(t.getTask().getId()));
                    final Coverage coverage = new Coverage(t.getTask().getId(), 1);
                    new UniqueResourceRequest<CoverageWrapper>(app.getSicmService())
                                .execute(app.getSicmService().createCoverage(new CoverageWrapper(coverage)));
                }
                taskChoisies.clear();
                startActivity(new Intent(ChoixTaches.this,ChoixVoies.class));
            }
        });

        ArrayList<TaskOP> list = ListeOperations.getOP(app, t);
        for(TaskOP top : list){
            if(top.getPosition().equals("FRONT")){
                front.add(top);
            } else {
                rear.add(top);
            }
        }

        Vector<View> pages = new Vector<>();
        if(!front.isEmpty()){
            final ListView listview1 = new ListView(this);
            listview1.setDivider(null);
            ListAdapter adapterListView1 = new AdapterTaches(this, R.layout.slide_row, front);
            listview1.setAdapter(adapterListView1);
            listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                    TaskOP top = (TaskOP) parent.getItemAtPosition(position);
                    LinearLayout border = (LinearLayout) ((LinearLayout) listview1.getChildAt(position)).getChildAt(0);
                    if(!taskChoisies.contains(top)){
                        taskChoisies.add((TaskOP) parent.getItemAtPosition(position));
                        border.setBackgroundColor(Color.RED);
                    } else {
                        taskChoisies.remove(top);
                        border.setBackgroundColor(Color.TRANSPARENT);
                    }
                }

            });
            pages.add(listview1);
        }

        if(!rear.isEmpty()){
            ListView listview2 = new ListView(this);
            listview2.setDivider(null);
            ListAdapter adapterListView2 = new AdapterTaches(this, R.layout.slide_row, rear);
            listview2.setAdapter(adapterListView2);
            listview2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                    TaskOP top = (TaskOP) parent.getItemAtPosition(position);
                    if(!taskChoisies.contains(top)){
                        taskChoisies.add((TaskOP) parent.getItemAtPosition(position));
                    } else {
                        taskChoisies.remove(top);
                    }
                }
            });
            pages.add(listview2);
        }

        ViewPager vp = (ViewPager) findViewById(R.id.viewPager);
        CustomPagerAdapter adapter = new CustomPagerAdapter(this,pages);
        vp.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(vp);
    }
}