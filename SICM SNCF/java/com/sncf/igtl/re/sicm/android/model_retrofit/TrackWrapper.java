package com.sncf.igtl.re.sicm.android.model_retrofit;

public class TrackWrapper {
    private Track track;

    public TrackWrapper(Track track) {
        this.track = track;
    }

    public Track getTrack() {
        return this.track;
    }
}
