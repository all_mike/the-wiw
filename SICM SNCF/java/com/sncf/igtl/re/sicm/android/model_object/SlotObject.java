package com.sncf.igtl.re.sicm.android.model_object;

public class SlotObject {
    private CarriageObject carriage;

    public SlotObject(CarriageObject carriage) {
        this.carriage = carriage;
    }

    public CarriageObject getCarriage() {
        return carriage;
    }

    public void setCarriage(CarriageObject carriage) {
        this.carriage = carriage;
    }
}
