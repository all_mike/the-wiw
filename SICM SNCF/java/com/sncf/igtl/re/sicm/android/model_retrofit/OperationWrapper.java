package com.sncf.igtl.re.sicm.android.model_retrofit;

public class OperationWrapper {
    private Operation operation;

    public OperationWrapper(Operation operation) {
        this.operation = operation;
    }

    public Operation getOperation() {
        return this.operation;
    }
}
