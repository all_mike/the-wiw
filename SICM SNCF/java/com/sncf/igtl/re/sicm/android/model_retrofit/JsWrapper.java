package com.sncf.igtl.re.sicm.android.model_retrofit;


public class JsWrapper<T> {

    private final T object;

    public JsWrapper(final T object) {
        this.object = object;
    }

    public T getValue() {
        return this.object;
    }
}
