package com.sncf.igtl.re.sicm.android.model_object;

import java.util.ArrayList;

public class VoieObject {
    private ArrayList<SlotObject> slots;

    public VoieObject() {
        this.slots = new ArrayList<>();
    }

    public ArrayList<SlotObject> getSlots() {
        return slots;
    }

}
