package com.sncf.igtl.re.sicm.android.model_retrofit;

public class Coverage {
    private int taskId;
    private int userId;
    private boolean pending;
    private boolean isPending;

    public Coverage(int taskId, int userId) {
        this.taskId = taskId;
        this.userId = userId;
    }
}
