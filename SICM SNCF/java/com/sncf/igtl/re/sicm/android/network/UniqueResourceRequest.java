package com.sncf.igtl.re.sicm.android.network;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;


public class UniqueResourceRequest<T> extends AsyncTask<Call<T>, Void, T> {

    private final SicmService sicmService;

    public UniqueResourceRequest(final SicmService sicmService) {
        this.sicmService = sicmService;
    }

    protected T doInBackground(Call<T>... calls) {
        try {
            final Response<T> response = calls[0].execute();
            Log.i("Request", String.valueOf(response.code()));
            return response.body();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return null;
    }
}
