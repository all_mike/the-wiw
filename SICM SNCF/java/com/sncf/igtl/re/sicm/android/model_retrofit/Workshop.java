package com.sncf.igtl.re.sicm.android.model_retrofit;

import java.util.Arrays;

public class Workshop {

    private int id;
    private String name;
    private int[] tracks;

    // CONSTRUCTEURS

    public Workshop(int id, String name, int[] tracks) {
        this.id = id;
        this.name = name;
        this.tracks = tracks;
    }

    // GETTERS

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int[] getTracks() {
        return tracks;
    }

    public String toString() {
        return "Workshop{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", tracks=" + Arrays.toString(tracks) +
                '}';
    }
}