package com.example.mikael.calculatrice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView tv;

    Button[] boutonsNumeriques;

    Button egal;
    Button c;

    Button plus;
    Button moins;
    Button multi;
    Button division;

    String res1 = "0";
    String res2 = "0";
    String calcul = "+";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = (TextView) findViewById(R.id.resultat);
        boutonsNumeriques = new Button[]{
                (Button) findViewById(R.id.zero),
                (Button) findViewById(R.id.un),
                (Button) findViewById(R.id.deux),
                (Button) findViewById(R.id.trois),
                (Button) findViewById(R.id.quatre),
                (Button) findViewById(R.id.cinq),
                (Button) findViewById(R.id.six),
                (Button) findViewById(R.id.sept),
                (Button) findViewById(R.id.huit),
                (Button) findViewById(R.id.neuf)
        };
        for(Button b : boutonsNumeriques){
            clickNombre(b);
        }

        plus = (Button) findViewById(R.id.plus);
        clickCalcul(plus);
        moins = (Button) findViewById(R.id.moins);
        clickCalcul(moins);
        multi = (Button) findViewById(R.id.multi);
        clickCalcul(multi);
        division = (Button) findViewById(R.id.division);
        clickCalcul(division);
        c = (Button) findViewById(R.id.c);
        c.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                res1="0";
                res2="0";
                tv.setText("");
            }
        });
        egal = (Button) findViewById(R.id.egal);
        egal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                res2 = tv.getText().toString();
                if(calcul.charAt(0) == '+'){
                    tv.setText(Integer.toString(Integer.parseInt(res1)+Integer.parseInt(res2)));
                } else {
                    if(calcul.charAt(0) == '-') {
                        tv.setText(Integer.toString(Integer.parseInt(res1)-Integer.parseInt(res2)));
                    } else {
                        if(calcul.charAt(0) == 'x'){
                            tv.setText(Integer.toString(Integer.parseInt(res1)*Integer.parseInt(res2)));
                        } else {
                            tv.setText(Double.toString(Double.parseDouble(res1)/Double.parseDouble(res2)));
                        }
                    }
                }
                res1 = "0";
                res2 = "0";
            }
        });
    }

    public void clickNombre(final Button bouton){
        bouton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String s = bouton.getText().toString();
                tv.setText(tv.getText()+s);
            }
        });
    }

    public void clickCalcul(final Button bouton){
        bouton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                res1 = tv.getText().toString();
                calcul = bouton.getText().toString();
                tv.setText("");
            }
        });
    }
}
